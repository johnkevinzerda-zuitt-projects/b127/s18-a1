let movies = [
	{
		title: 'Toy Story',
		genre: 'Family/Comedy',
		releaseDate: new Date(1995, 10, 19),
		rating: 8.5,
		displayRating: function(){
		console.log('The movie'+' '+this.title+' '+'has'+' '+this.rating+' '+'out of 10 stars.')
		},
		titleAndGenre: function(){
			console.log(this.title+' '+'is'+' '+'a'+' '+this.genre+' '+'movie')
		}
	},

	{
		title: 'The Happening',
		genre: 'Horror/Thriller',
		releaseDate: new Date(2008, 5, 10),
		rating: 5,
		displayRating: function(){
		console.log('The movie'+' '+this.title+' '+'has'+' '+this.rating+' '+'out of 10 stars.')
		},
		titleAndGenre: function(){
			console.log(this.title+' '+'is'+' '+'a'+' '+this.genre+' '+'movie')
		}
	},

	{
		title: 'Like Stars on Earth',
		genre: 'Drama/Musical',
		releaseDate: new Date(2007, 11, 21),
		rating: 8.4,
		displayRating: function(){
		console.log('The movie'+' '+this.title+' '+'has'+' '+this.rating+' '+'out of 10 stars.')
		},
		titleAndGenre: function(){
			console.log(this.title+' '+'is'+' '+'a'+' '+this.genre+' '+'movie')
		}
	},

	{
		title: 'The Blind Side',
		genre: 'Sport/Drama',
		releaseDate: new Date(2009, 10, 20),
		rating: 7.6,
		displayRating: function(){
		console.log('The movie'+' '+this.title+' '+'has'+' '+this.rating+' '+'out of 10 stars.')
		},
		titleAndGenre: function(){
			console.log(this.title+' '+'is'+' '+'a'+' '+this.genre+' '+'movie')
		}
	},

	{
		title: 'Avatar',
		genre: 'Sci-fi',
		releaseDate: new Date(2009, 11, 10),
		rating: 7.8,
		displayRating: function(){
		console.log('The movie'+' '+this.title+' '+'has'+' '+this.rating+' '+'out of 10 stars.')
		},
		titleAndGenre: function(){
			console.log(this.title+' '+'is'+' '+'a'+' '+this.genre+' '+'movie')
		}
	}
]

function displayAllMovies(arr){
	for (i = 0; i <arr.length; i++) {
		movies[i].titleAndGenre();
	}

}


//test_case

movies[0].displayRating();
movies[1].displayRating();
movies[2].displayRating();
movies[3].displayRating();
movies[4].displayRating();

displayAllMovies(movies);
